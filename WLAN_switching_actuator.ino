#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>
#include <Ticker.h>
#include <AsyncMqttClient.h>






//**************** User defined parameters *******************
#define WIFI_SSID "XXXXX"
#define WIFI_PASSWORD "XXXXX"

#define MQTT_server IPAddress(x,x,x,x)
#define MQTT_PORT 1883
#define MQTT_userName "XXXXX"
#define MQTT_pass "XXXXX"

const char* host = "esp8266-webupdatexx";
#define MQTT_clientID "XXXXX"
#define mqtt_topic_lastwill "/swxxx_online"
#define mqtt_topic "/swxxx"
#define mqtt_tpoic_status "/swxx_status"
//************************************************************






AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

#define Relay D4 //Triac output
#define Button D3 //Switch 

volatile int buttonState;//Must be volatile when used in interrupt function
volatile int RelayState;
String strRelayState;

long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 100;    // the debounce time; increase if the output flickers

/********************** interrupt Function ************************************/
void handSwitch()
{
    detachInterrupt(digitalPinToInterrupt(Button)); 
    lastDebounceTime = millis();
    while ((millis() - lastDebounceTime) < debounceDelay) {
      //donothing
    }
    //delay(50);
    if (buttonState != digitalRead(Button))
    {
    //update button state
    buttonState = digitalRead(Button);
    //switch to opposite value
    digitalWrite(Relay, !RelayState);
    //update relay state
    RelayState=digitalRead(Relay);  
    //Publish
    if (RelayState == 0)
    {
      mqttClient.publish(mqtt_tpoic_status, 1, true, "1");//switch off relay
    }
    else if (RelayState == 1)
    {
      mqttClient.publish(mqtt_tpoic_status, 1, true, "0");
    }
    // print out the states:
    Serial.print("  buttonState: ");
    Serial.println(buttonState);
    Serial.print("  RelayState: ");
    Serial.println(RelayState);
    }
    //Define Interrupt function
    attachInterrupt(digitalPinToInterrupt(Button),handSwitch,CHANGE);
  }

  void PublishCurrentState() {
    //----------Important: publish current status
    //update ReedSwitch1_pin state
    RelayState=digitalRead(Relay);
    //Publish (There is problem converting integer into string)
    if (RelayState == 0)
    {
      mqttClient.publish(mqtt_tpoic_status, 1, true, "1");//switch off relay
    }
    else if (RelayState == 1)
    {
      mqttClient.publish(mqtt_tpoic_status, 1, true, "0");
    }  
}

/********************** Async MQTT Client ************************************/
void connectToWifi() {
  Serial.println("Connecting to Wi-Fi...");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void connectToMqtt() {
  Serial.println("Connecting to MQTT...");
  mqttClient.connect();
}

void onWifiConnect(const WiFiEventStationModeGotIP& event) {
  Serial.println("Connected to Wi-Fi.");
  connectToMqtt();
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected& event) {
  Serial.println("Disconnected from Wi-Fi.");
  mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
  wifiReconnectTimer.once(2, connectToWifi);
}

void onMqttConnect(bool sessionPresent) {
  Serial.println("Connected to MQTT.");
  Serial.print("Session present: ");
  Serial.println(sessionPresent);
  uint16_t packetIdSub = mqttClient.subscribe(mqtt_topic, 1);
  Serial.print("Subscribing at QoS 1, packetId: ");
  Serial.println(packetIdSub);
   //Submit online status
  uint16_t packetIdPub1 = mqttClient.publish(mqtt_topic_lastwill, 1, true, "yes");
  Serial.print("Publishing at QoS 1, packetId: ");
  Serial.println(packetIdPub1);
  //Publish on every mqtt connect
  PublishCurrentState();//only one time
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  Serial.println("Disconnected from MQTT.");

  if (WiFi.isConnected()) {
    Serial.println("Connecting to MQTT...");
    mqttReconnectTimer.once(2, connectToMqtt);
  }
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
  Serial.println("Subscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
  Serial.print("  qos: ");
  Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId) {
  Serial.println("Unsubscribe acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  Serial.println("** Publish received **");
  Serial.print("  topic: ");
  Serial.println(topic);
  Serial.print("  payload: ");
  Serial.println(payload);
  Serial.print("  qos: ");
  Serial.println(properties.qos);
  Serial.print("  dup: ");
  Serial.println(properties.dup);
  Serial.print("  retain: ");
  Serial.println(properties.retain);
  Serial.print("  len: ");
  Serial.println(len);
  Serial.print("  index: ");
  Serial.println(index);
  Serial.print("  total: ");
  Serial.println(total);
  
  //turn the light on if the payload is '1' and publish to the MQTT server a confirmation message
  if(payload[0] == '1'){
    //digitalWrite(Relay, !RelayState);
    digitalWrite(Relay, 0);
    //update relay state
    RelayState=digitalRead(Relay);  
    Serial.print("  RelayState-MQTT: ");
    Serial.println(RelayState);
  }
  else if (payload[0] == '0'){
     digitalWrite(Relay, 1);
    //update relay state
    RelayState=digitalRead(Relay);  
    Serial.print("  RelayState-MQTT: ");
    Serial.println(RelayState);
  }
  //Publish (There is problem converting integer into string)
  if (RelayState == 0)
  {
    mqttClient.publish(mqtt_tpoic_status, 1, true, "1");//switch off relay
  }
  else if (RelayState == 1)
  {
    mqttClient.publish(mqtt_tpoic_status, 1, true, "0");
  }
}

void onMqttPublish(uint16_t packetId) {
  Serial.println("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;

void setup() {
  Serial.begin(115200);
	
  delay(100);//let power supply stablize
  
  pinMode(Relay, OUTPUT);
  digitalWrite(Relay, HIGH);
  //digitalWrite(Relay, HIGH);
  RelayState=digitalRead(Relay);
  Serial.print("  Initial Relay: ");
  Serial.println(RelayState);
  
  pinMode(Button, INPUT_PULLUP);
  buttonState = digitalRead(Button);
  Serial.print("  Initial buttonState: ");
  Serial.println(buttonState);

  //Define Interr1upt function
  attachInterrupt(digitalPinToInterrupt(Button),handSwitch,CHANGE);
    
  Serial.println();
  Serial.println("Booting Sketch...");
  
   wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
  wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);
  
  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onUnsubscribe(onMqttUnsubscribe);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.onPublish(onMqttPublish);
  mqttClient.setServer(MQTT_server, 1883);
  mqttClient.setKeepAlive(15).setCleanSession(true).setWill(mqtt_topic_lastwill, 1, true, "no").setCredentials(MQTT_userName, MQTT_pass).setClientId(MQTT_clientID);
  Serial.println("Connecting to MQTT...");

  connectToWifi();

  MDNS.begin(host);

  httpUpdater.setup(&httpServer);
  httpServer.begin();  //Start server

  MDNS.addService("http", "tcp", 80);
  Serial.printf("HTTPUpdateServer ready! Open http://%s.local/update in your browser\n", host);
}

void loop() {
   httpServer.handleClient();   //Handle client requests
   yield();
  }

